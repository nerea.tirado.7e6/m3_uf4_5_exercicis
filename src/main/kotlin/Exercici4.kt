/*
//4.Quiz
abstract class Question(val text: String) {
    abstract fun checkAnswer(answer: String): Boolean
}

class FreeTextQuestion(text: String, private val correctAnswer: String) : Question(text) {
    override fun checkAnswer(answer: String): Boolean {
        return answer == correctAnswer
    }
}

class MultipleChoiceQuestion(text: String, private val choices: List<String>, private val correctAnswerIndex: Int) : Question(text) {
    override fun checkAnswer(answer: String): Boolean {
        return answer.toIntOrNull() == correctAnswerIndex
    }

    fun printChoices() {
        for ((index, choice) in choices.withIndex()) {
            println("$index) $choice")
        }
    }
}

class Quiz(private val questions: List<Question>) {
    fun run() {
        var correctAnswers = 0
        for (question in questions) {
            println(question.text)
            when (question) {
                is FreeTextQuestion -> {
                    val answer = readLine() ?: ""
                    if (question.checkAnswer(answer)) {
                        correctAnswers++
                    }
                }
                is MultipleChoiceQuestion -> {
                    question.printChoices()
                    val answer = readLine() ?: ""
                    if (question.checkAnswer(answer)) {
                        correctAnswers++
                    }
                }
            }
        }
        println("Tienes $correctAnswers de ${questions.size} correctas!")
    }
}
fun main(args: Array<String>) {
    val quiz = Quiz(
        listOf(
            FreeTextQuestion("Capital de Francia?", "Paris"),
            MultipleChoiceQuestion("Cual es el rio mas largo?",
                listOf("Amazonas", "Yangzi", "Huang He", "Nilo"), 0),
            FreeTextQuestion("Animal mas alto?", "Jirafa")
        )
    )
    quiz.run()
}
*/
