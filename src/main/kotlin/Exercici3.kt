//import javax.print.attribute.standard.MediaSizeName.A
//
////3.InstrumentSimulator
//abstract class Instrument(){
//    abstract val resonance: Any
//    abstract fun makeSounds(repeticion: Int)
//}
//class Triangle(override val resonance: Any): Instrument() {
//    override fun makeSounds(repeticion: Int) {
//       val sound = when (resonance){
//           1 -> "TINC"
//           2 -> "TIINC"
//           3 -> "TIIINC"
//           4 -> "TIIIINC"
//           5 -> "TIIIIINC"
//           else -> throw IllegalArgumentException("Resonancia no válida: $resonance")
//       }
//        repeat(repeticion){
//            println(sound)
//        }
//    }
//}
//class Drump(override val resonance: Any): Instrument() {
//    override fun makeSounds(repeticion: Int) {
//       val sound = when (resonance){
//           "A" -> "TAAAM"
//           "O" -> "TOOOM"
//           "U" -> "TUUUM"
//           else -> throw IllegalArgumentException("Resonancia no valida: $resonance")
//       }
//        repeat(repeticion){
//            println(sound)
//        }
//    }
//}
//fun main(args: Array<String>) {
//    val instruments: List<Instrument> = listOf(
//        Triangle(5),
//        Drump("A"),
//        Drump("O"),
//        Triangle(1),
//        Triangle(5)
//    )
//    play(instruments)
//}
//
//private fun play(instruments: List<Instrument>) {
//    for (instrument in instruments) {
//        instrument.makeSounds(2)
//    }
//}
